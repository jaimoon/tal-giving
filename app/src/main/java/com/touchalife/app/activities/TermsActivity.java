package com.touchalife.app.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.touchalife.app.R;

public class TermsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms);
    }
}