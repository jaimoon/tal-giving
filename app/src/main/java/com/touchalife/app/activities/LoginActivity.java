package com.touchalife.app.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.touchalife.app.MainActivity;
import com.touchalife.app.R;
import com.touchalife.app.databinding.ActivityLoginBinding;
import com.touchalife.app.network.RetrofitClient;
import com.touchalife.app.responses.EmailValidateResponse;
import com.touchalife.app.responses.LoginResponse;

import retrofit2.Call;
import retrofit2.Callback;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    ActivityLoginBinding binding;
    String email,password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_login);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login);

        binding.btnLogin.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        email = binding.etEmail.getText().toString();
        password = binding.etPassword.getText().toString();

        userLogin();

        emailValidate();

    }

    private void emailValidate() {

        Call<EmailValidateResponse> call = RetrofitClient.getInstance().getApi().emailValidate(email);
        call.enqueue(new Callback<EmailValidateResponse>() {
            @Override
            public void onResponse(Call<EmailValidateResponse> call, retrofit2.Response<EmailValidateResponse> response) {


                EmailValidateResponse loginResponse = response.body();

                assert loginResponse != null;
                if (loginResponse.getStatusCode() == 200) {

                    Toast.makeText(LoginActivity.this, "Success", Toast.LENGTH_SHORT).show();

                    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                    startActivity(intent);


                } else if (loginResponse.getStatus().equals("400")) {

                    Toast.makeText(LoginActivity.this, loginResponse.getMessage(), Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<EmailValidateResponse> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
                // pprogressDialog.dismiss();
            }
        });

    }

    private void userLogin() {

        ProgressDialog progressDialog = new ProgressDialog(LoginActivity.this);
        progressDialog.setMessage("Loading....");
        progressDialog.show();

        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("account", email);
        jsonObject.addProperty("password", password);

        Call<LoginResponse> call1= RetrofitClient.getInstance().getApi().userLogin(jsonObject);
        call1.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, retrofit2.Response<LoginResponse> response) {
                if (response.isSuccessful()) ;
                progressDialog.dismiss();
                LoginResponse loginResponse = response.body();

                assert loginResponse != null;
                if (loginResponse.getStatusCode() == 200) {

                    Toast.makeText(LoginActivity.this, "Success", Toast.LENGTH_SHORT).show();

                    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                    startActivity(intent);


                } else if (loginResponse.getStatus().equals("400")) {

                    Toast.makeText(LoginActivity.this, loginResponse.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {

                Toast.makeText(LoginActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });

    }

}