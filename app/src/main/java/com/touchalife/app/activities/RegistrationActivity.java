package com.touchalife.app.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;
import com.google.gson.JsonObject;
import com.hbb20.CountryCodePicker;
import com.touchalife.app.R;
import com.touchalife.app.databinding.ActivityRegistrationBinding;
import com.touchalife.app.network.RetrofitClient;
import com.touchalife.app.responses.RegResponse;
import com.touchalife.app.utils.NetworkChecking;

import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegistrationActivity extends AppCompatActivity implements View.OnClickListener {

    ActivityRegistrationBinding binding;
    String firstName, lastName, email, password, mobile;
    boolean showPassword = false;
    Pattern pattern;
    Matcher matcher;
    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    private boolean checkInternet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_registration);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_registration);

        checkInternet = NetworkChecking.isConnected(this);

        binding.btnRegister.setOnClickListener(this);
        binding.llLogin.setOnClickListener(this);
        binding.etPassword.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;

                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (event.getRawX() >= (binding.etPassword.getRight() - binding.etPassword.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        // your action here

                        if (!showPassword) {
                            showPassword = true;
                            binding.etPassword.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_edit_pass, 0, R.drawable.ic_edit_red_eye, 0);
                            binding.etPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                        } else {
                            showPassword = false;
                            binding.etPassword.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_edit_pass, 0, R.drawable.ic_edit_eye, 0);
                            binding.etPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
                        }

                    }
                }
                return false;
            }
        });
        binding.reffCode.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionID, KeyEvent keyEvent) {
                if (actionID == EditorInfo.IME_ACTION_DONE) {
                    userRegistration();
                }

                return false;
            }
        });

        binding.ccp.setOnCountryChangeListener(new CountryCodePicker.OnCountryChangeListener() {
            @Override
            public void onCountrySelected() {
                String selected_country_code = binding.ccp.getSelectedCountryCodeWithPlus();
                Log.d("SelectedCountry", selected_country_code);
            }
        });

    }

    @Override
    public void onClick(View v) {

        if (v == binding.termsText) {
            if (checkInternet) {
                Intent intent = new Intent(RegistrationActivity.this, TermsActivity.class);
                startActivity(intent);
            } else {
                Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), "Check Internet Connection", Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        }

        if (v == binding.btnRegister) {

            firstName = binding.etFirstName.getText().toString();
            lastName = binding.etLastName.getText().toString();
            email = binding.etEmail.getText().toString();
            password = binding.etPassword.getText().toString();
            mobile = binding.etMobile.getText().toString();

            if (checkInternet) {

                if (Validation()) {
                    userRegistration();
                }
            } else {
                Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), "Check Internet Connection", Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        }

        if (v == binding.llLogin) {
            if (checkInternet) {
                Intent intent = new Intent(RegistrationActivity.this, LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
            } else {
                Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), "Check Internet Connection", Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        }

    }

    private void userRegistration() {

        ProgressDialog progressDialog = new ProgressDialog(RegistrationActivity.this);
        progressDialog.setMessage("Loading....");
        progressDialog.show();

        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("first_name", firstName);
        jsonObject.addProperty("last_name", lastName);
        jsonObject.addProperty("email", email);
        jsonObject.addProperty("password", password);
        jsonObject.addProperty("phone", mobile);

        Call<RegResponse> call = RetrofitClient.getInstance().getApi().userRegistrationRequest(jsonObject);
        call.enqueue(new Callback<RegResponse>() {
            @Override
            public void onResponse(Call<RegResponse> call, Response<RegResponse> response) {
                progressDialog.dismiss();
                RegResponse registerResponse = response.body();
                assert registerResponse != null;
                if (registerResponse.getStatusCode() == 200) {

                    Log.d("Check", String.valueOf(registerResponse));
                    Toast.makeText(RegistrationActivity.this, "Success", Toast.LENGTH_SHORT).show();

                    //emailVerficationService();


                } else if (registerResponse.getStatusCode() == 400) {
                    Toast.makeText(RegistrationActivity.this, registerResponse.getMessage(), Toast.LENGTH_SHORT).show();
                }


            }

            @Override
            public void onFailure(Call<RegResponse> call, Throwable t) {
                progressDialog.dismiss();
            }
        });

    }

    public boolean Validation() {

        if (binding.etEmail.getText().toString().isEmpty()) {
            binding.etEmail.setError("Enter valid email address");
            return false;
        } else if (!binding.etEmail.getText().toString().trim().matches(emailPattern)) {
            binding.etEmail.setError("Invalid email address");
            return false;
        }

        if (binding.etFirstName.getText().toString().isEmpty()) {
            binding.etFirstName.setError("Enter valid first name");
            return false;
        }

        if (binding.etLastName.getText().toString().isEmpty()) {
            binding.etLastName.setError("Enter valid last name");
            return false;
        }

        if (binding.etMobile.getText().toString().isEmpty()) {
            binding.etMobile.setError("Enter valid mobile number");
            return false;
        }

        if (binding.etPassword.getText().toString().isEmpty()) {
            binding.etPassword.setError("Enter valid password");
            return false;
        } else if (binding.etPassword.getText().toString().length() < 8) {
            binding.etPassword.setError("Please enter minimun 8 char password");
            return false;
        } else if (!validate(binding.etPassword.getText().toString())) {
            binding.etPassword.setError("Please enter a valid password");
            return false;
        }
        return true;
    }

    public boolean validate(final String password) {
        final String PASSWORD_PATTERN = "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*(?=.*[!-\\/:-@\\[-`{-~])).{6,20})";
        pattern = Pattern.compile(PASSWORD_PATTERN);
        matcher = pattern.matcher(password);
        return matcher.matches();

    }

    /*public void goToEmailVerification(Context context) {
        otpVerificationFrom = "Register";
        MyApplication.getInstance().setOtpVerificationFrom(otpVerificationFrom);
        Intent intent = new Intent(context, OTPVerification.class);
        startActivity(intent);
    }

    public void emailVerficationService(){

        String url = URL.base_url+URL.email_verification+MyApplication.getInstance().getUser_email();

        new CallServiceNoProgressBar(RegistrationActivity.this, url, ServiceHandler.GET, new ServiceCallback() {
            @Override
            public void response(String res) {
                System.out.println("response ::::" +res);
                progressDialog.dismiss();
                goToEmailVerification(RegistrationActivity.this);
            }
        }).execute();
    }*/
}