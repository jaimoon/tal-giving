package com.touchalife.app.responses;

import java.util.List;

public class EmailValidateResponse {

    /**
     * status : success
     * statusCode : 200
     * message : User data
     * data : {"unique_id":"5f02b3be96280968ebeb4041","email":"jayachandra.k@touchalife.org","phone":"","username":"jaykalipalli","display_name":"","name":{"middle_name":"","first_name":"Jay","last_name":"Kalipalli"},"profile_image_url":"","account_verified":true,"account_status":1,"password_verified":true,"login_provider":"local","email_verified":false,"phone_verified":false,"address":{"line1":"","line2":"","city":"","state":"","country":""},"roles":["donor","donee"],"social_verification":{"google_verified":false,"facebook_verified":false,"linkedin_verified":false,"twitter_verified":false,"apple_verified":false},"token_detail":{"token":"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjoiNWYwMmIzYmU5NjI4MDk2OGViZWI0MDQxIiwiaWF0IjoxNTk0NjM5OTkxLCJleHAiOjE1OTQ3MjYzOTF9.Zrzi3RP6UgsiW-dfwGZwlA1HpugR-4YyQiEBiYEjmAw","type":"Bearer"},"summary":"","gender":"","dob":0,"stripeCustomerId":"","kindness_score":95,"volunteerInfo":{},"rating":1,"isTourCompleted":false,"otp":864992}
     */

    private String status;
    private int statusCode;
    private String message;
    private DataBean data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * unique_id : 5f02b3be96280968ebeb4041
         * email : jayachandra.k@touchalife.org
         * phone :
         * username : jaykalipalli
         * display_name :
         * name : {"middle_name":"","first_name":"Jay","last_name":"Kalipalli"}
         * profile_image_url :
         * account_verified : true
         * account_status : 1
         * password_verified : true
         * login_provider : local
         * email_verified : false
         * phone_verified : false
         * address : {"line1":"","line2":"","city":"","state":"","country":""}
         * roles : ["donor","donee"]
         * social_verification : {"google_verified":false,"facebook_verified":false,"linkedin_verified":false,"twitter_verified":false,"apple_verified":false}
         * token_detail : {"token":"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjoiNWYwMmIzYmU5NjI4MDk2OGViZWI0MDQxIiwiaWF0IjoxNTk0NjM5OTkxLCJleHAiOjE1OTQ3MjYzOTF9.Zrzi3RP6UgsiW-dfwGZwlA1HpugR-4YyQiEBiYEjmAw","type":"Bearer"}
         * summary :
         * gender :
         * dob : 0
         * stripeCustomerId :
         * kindness_score : 95
         * volunteerInfo : {}
         * rating : 1
         * isTourCompleted : false
         * otp : 864992
         */

        private String unique_id;
        private String email;
        private String phone;
        private String username;
        private String display_name;
        private NameBean name;
        private String profile_image_url;
        private boolean account_verified;
        private int account_status;
        private boolean password_verified;
        private String login_provider;
        private boolean email_verified;
        private boolean phone_verified;
        private AddressBean address;
        private SocialVerificationBean social_verification;
        private TokenDetailBean token_detail;
        private String summary;
        private String gender;
        private int dob;
        private String stripeCustomerId;
        private int kindness_score;
        private VolunteerInfoBean volunteerInfo;
        private int rating;
        private boolean isTourCompleted;
        private int otp;
        private List<String> roles;

        public String getUnique_id() {
            return unique_id;
        }

        public void setUnique_id(String unique_id) {
            this.unique_id = unique_id;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getDisplay_name() {
            return display_name;
        }

        public void setDisplay_name(String display_name) {
            this.display_name = display_name;
        }

        public NameBean getName() {
            return name;
        }

        public void setName(NameBean name) {
            this.name = name;
        }

        public String getProfile_image_url() {
            return profile_image_url;
        }

        public void setProfile_image_url(String profile_image_url) {
            this.profile_image_url = profile_image_url;
        }

        public boolean isAccount_verified() {
            return account_verified;
        }

        public void setAccount_verified(boolean account_verified) {
            this.account_verified = account_verified;
        }

        public int getAccount_status() {
            return account_status;
        }

        public void setAccount_status(int account_status) {
            this.account_status = account_status;
        }

        public boolean isPassword_verified() {
            return password_verified;
        }

        public void setPassword_verified(boolean password_verified) {
            this.password_verified = password_verified;
        }

        public String getLogin_provider() {
            return login_provider;
        }

        public void setLogin_provider(String login_provider) {
            this.login_provider = login_provider;
        }

        public boolean isEmail_verified() {
            return email_verified;
        }

        public void setEmail_verified(boolean email_verified) {
            this.email_verified = email_verified;
        }

        public boolean isPhone_verified() {
            return phone_verified;
        }

        public void setPhone_verified(boolean phone_verified) {
            this.phone_verified = phone_verified;
        }

        public AddressBean getAddress() {
            return address;
        }

        public void setAddress(AddressBean address) {
            this.address = address;
        }

        public SocialVerificationBean getSocial_verification() {
            return social_verification;
        }

        public void setSocial_verification(SocialVerificationBean social_verification) {
            this.social_verification = social_verification;
        }

        public TokenDetailBean getToken_detail() {
            return token_detail;
        }

        public void setToken_detail(TokenDetailBean token_detail) {
            this.token_detail = token_detail;
        }

        public String getSummary() {
            return summary;
        }

        public void setSummary(String summary) {
            this.summary = summary;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public int getDob() {
            return dob;
        }

        public void setDob(int dob) {
            this.dob = dob;
        }

        public String getStripeCustomerId() {
            return stripeCustomerId;
        }

        public void setStripeCustomerId(String stripeCustomerId) {
            this.stripeCustomerId = stripeCustomerId;
        }

        public int getKindness_score() {
            return kindness_score;
        }

        public void setKindness_score(int kindness_score) {
            this.kindness_score = kindness_score;
        }

        public VolunteerInfoBean getVolunteerInfo() {
            return volunteerInfo;
        }

        public void setVolunteerInfo(VolunteerInfoBean volunteerInfo) {
            this.volunteerInfo = volunteerInfo;
        }

        public int getRating() {
            return rating;
        }

        public void setRating(int rating) {
            this.rating = rating;
        }

        public boolean isIsTourCompleted() {
            return isTourCompleted;
        }

        public void setIsTourCompleted(boolean isTourCompleted) {
            this.isTourCompleted = isTourCompleted;
        }

        public int getOtp() {
            return otp;
        }

        public void setOtp(int otp) {
            this.otp = otp;
        }

        public List<String> getRoles() {
            return roles;
        }

        public void setRoles(List<String> roles) {
            this.roles = roles;
        }

        public static class NameBean {
            /**
             * middle_name :
             * first_name : Jay
             * last_name : Kalipalli
             */

            private String middle_name;
            private String first_name;
            private String last_name;

            public String getMiddle_name() {
                return middle_name;
            }

            public void setMiddle_name(String middle_name) {
                this.middle_name = middle_name;
            }

            public String getFirst_name() {
                return first_name;
            }

            public void setFirst_name(String first_name) {
                this.first_name = first_name;
            }

            public String getLast_name() {
                return last_name;
            }

            public void setLast_name(String last_name) {
                this.last_name = last_name;
            }
        }

        public static class AddressBean {
            /**
             * line1 :
             * line2 :
             * city :
             * state :
             * country :
             */

            private String line1;
            private String line2;
            private String city;
            private String state;
            private String country;

            public String getLine1() {
                return line1;
            }

            public void setLine1(String line1) {
                this.line1 = line1;
            }

            public String getLine2() {
                return line2;
            }

            public void setLine2(String line2) {
                this.line2 = line2;
            }

            public String getCity() {
                return city;
            }

            public void setCity(String city) {
                this.city = city;
            }

            public String getState() {
                return state;
            }

            public void setState(String state) {
                this.state = state;
            }

            public String getCountry() {
                return country;
            }

            public void setCountry(String country) {
                this.country = country;
            }
        }

        public static class SocialVerificationBean {
            /**
             * google_verified : false
             * facebook_verified : false
             * linkedin_verified : false
             * twitter_verified : false
             * apple_verified : false
             */

            private boolean google_verified;
            private boolean facebook_verified;
            private boolean linkedin_verified;
            private boolean twitter_verified;
            private boolean apple_verified;

            public boolean isGoogle_verified() {
                return google_verified;
            }

            public void setGoogle_verified(boolean google_verified) {
                this.google_verified = google_verified;
            }

            public boolean isFacebook_verified() {
                return facebook_verified;
            }

            public void setFacebook_verified(boolean facebook_verified) {
                this.facebook_verified = facebook_verified;
            }

            public boolean isLinkedin_verified() {
                return linkedin_verified;
            }

            public void setLinkedin_verified(boolean linkedin_verified) {
                this.linkedin_verified = linkedin_verified;
            }

            public boolean isTwitter_verified() {
                return twitter_verified;
            }

            public void setTwitter_verified(boolean twitter_verified) {
                this.twitter_verified = twitter_verified;
            }

            public boolean isApple_verified() {
                return apple_verified;
            }

            public void setApple_verified(boolean apple_verified) {
                this.apple_verified = apple_verified;
            }
        }

        public static class TokenDetailBean {
        }

        public static class VolunteerInfoBean {
        }
    }
}
